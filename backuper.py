#!/usr/bin/env python3
import os
import time
import subprocess
from pathlib import Path
import argparse
import datetime
import xml.etree.ElementTree
import shutil

# Путь к директории с бекапами
backup_path = "/home/andrey/backup/"
# Кол-во бекапов
backup_counts = 4
# Ожидание времени выключения ВМ перед бекапом, сек
vm_shutdown_wait_time = 100
# Свободное место на диске, необходимое для бекапа, МБ
# Если ф-ция не нужна - выставить 0
free_space = 200
backups = {}
backups_dates = []
vm = {}
vm_initial = {}


def check_space(drive):
    drive = (shutil.disk_usage(drive)[2] // 1024 // 1024)
    if drive < free_space:
        print('Error. Free space on', backup_path, 'is not sufficient for backup')
        exit(1)
    return


def check_backups():
    global backups_dates
    global backups
    if os.listdir(backup_path):
        for bdir in Path(backup_path).iterdir():
            if str(bdir).find(args.vmname) != -1:
                backups[int(Path(bdir).stat().st_mtime)] = str(bdir)
                backups_dates.append(int(Path(bdir).stat().st_mtime))
                # print(str(bdir), Path(bdir).stat().st_mtime)
                # if 'cirros'.find(Path(bdir).parts[len(Path(bdir).parts)-1:]):print('!!')
                # if str(bdir).find(args.vmname) != -1: print('!!')
        backups_dates.sort()
    # print(datetime.datetime.fromtimestamp(int(Path(bdir).stat().st_mtime)).strftime('%Y-%m-%d %H:%M:%S'))
    # print(backups," ",datetime.datetime.fromtimestamp(backups['/home/andrey/backup/1']).strftime('%Y-%m-%d %H:%M:%S'))
    return


def do_rotate():
    global backups_dates
    global backups
    check_backups()
    if len(backups_dates) > 0:
        if len(backups_dates) > backup_counts:
            print("Going to rotate", len(backups_dates) - backup_counts, "copies.")
            for i in range(0, len(backups_dates) - backup_counts):
                print('Removing backup', backups[backups_dates[i]])
                shutil.rmtree(backups[backups_dates[i]], ignore_errors=True)
        else:
            print("Nothing to rotate, keeping", backup_counts, "copies - for now i have", len(backups_dates), "copies.")
    else:
        print("Nothing to rotate, directory", backup_path, "is empty.")
    return


def get_vmstate():
    global vm
    try:
        vout = subprocess.check_output(["virsh", "list", "--all"], timeout=20, shell=False,
                                       universal_newlines=True).lower()
    except subprocess.TimeoutExpired:
        print("Error. Can't get VM status - timeout!")
    except subprocess.CalledProcessError:
        print("Virsh Error!")
    mylist = vout.split("\n")[2:][:-2]
    s = []
    for i in range(len(mylist)):
        s.append(mylist[i].split()[1:][:2])
    vm = dict((key, value) for (key, value) in s)
    if args.vmname not in list(vm.keys()):
        print('Error. VM not found:', args.vmname)
        exit(1)
    return


def do_backup():
    global vm_initial
    timer = 0
    vm_initial = vm
    global backup_subdir
    backup_subdir = backup_path + datetime.datetime.now().strftime('%Y-%m-%d-%H%M') + '.' + args.vmname + '/'
    try:
        os.makedirs(backup_subdir, 0o770, exist_ok=False)
    except:
        print("Can't create", backup_subdir)
        exit(1)
    if vm_initial[args.vmname] == 'running':
        print('Trying to stop VM', args.vmname)
        subprocess.check_call(["virsh", "shutdown", args.vmname])
        while vm_initial[args.vmname] == vm[args.vmname]:
            time.sleep(1)
            get_vmstate()
            timer += 1
            if timer >= vm_shutdown_wait_time:
                print('Error stopping', args.vmname, 'waited', timer, 'sec!')
                exit(1)
                ####backup here
    do_filecopy()
    ####backup here
    if vm_initial[args.vmname] == 'running':
        print('Starting previously stopped VM', args.vmname)
        try:
            subprocess.check_call(["virsh", "start", args.vmname], shell=False)
        except subprocess.CalledProcessError:
            print("Can't start VM!", args.vmname)
    elif vm_initial[args.vmname] == 'shut':
        print('Will not start VM', args.vmname, 'because its status was down')
    return


def do_filecopy():
    # backup_subdir = backup_path + datetime.datetime.now().strftime('%Y-%m-%d-%H%M') + '.'+args.vmname+'/'
    xml_conf = backup_subdir + args.vmname + '.xml'
    # try:
    #    os.makedirs(backup_subdir, 0o770, exist_ok=True)
    # except:
    #    print("Can't create", backup_subdir)
    try:
        os.system('virsh dumpxml ' + args.vmname + ' > ' + xml_conf)
    except:
        print('Error dumping', args.vmname, 'VM config')
    vm_conf = xml.etree.ElementTree.parse(xml_conf).getroot()
    drivelist = []
    for atype in vm_conf.iter('source'):
        if atype.attrib.get('file') != None: drivelist.append(atype.attrib.get('file'))
    for drive_img in drivelist:
        try:
            print('Compressing', drive_img)
            subprocess.check_call(['lzop', '-f', '-p' + backup_subdir, drive_img], shell=False)
        except subprocess.CalledProcessError:
            print("Can't compress img", drive_img, "to", backup_subdir)
    return


def do_init():
    if backup_counts < 1:
        print("Error. Number of backup copies can't be zero or less!")
        exit(1)
    if os.getgid() > 0:
        print('Error. You must be root to run this script!')
        exit(1)
    return


parser = argparse.ArgumentParser(description='Backuping kvm virtual machines.')
parser.add_argument('vmname', type=str, help='VM name to backup')
args = parser.parse_args()

do_init()
check_space(backup_path)
get_vmstate()
do_backup()
do_rotate()

exit(0)
